/**
 * Created by sunbiao on 2017/8/7.
 */
import React, { PureComponent } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Image,
    Dimensions,
    TextInput,
    ScrollView,
    FlatList,
    SectionList,
    RefreshControl,
    TouchableOpacity,

} from 'react-native';
import NavigationItem from '../../widget/NavigationItem';

export default class  MineScene extends PureComponent{
    static navigationOptions = ({navigation})=>({
        headerTitle:'个人信息',
        headerLeft:(
            <NavigationItem
                icon={require('../../img/Mine/icon_mine_collection.png')}
                onPress={()=>{
                    navigation.goBack();
                }}
            />
        ),
    })




    render(){
        return(
            <View>
                <Text>this is a mineInfo</Text>
            </View>
        );
    }
}