/**
 * Created by sunbiao on 2017/7/24.
 */
/**
 * Created by sunbiao on 2017/7/24.
 */
import React, { PureComponent } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Image,
    Dimensions,
    TextInput,
    ScrollView,
    FlatList,
    SectionList,
    RefreshControl,
TouchableOpacity,

} from 'react-native';
import Button from '../../widget/Button';
import NavigationItem from  '../../widget/NavigationItem';
import  screen from '../../common/screen'
import  color from '../../widget/color'
import { Heading1, Heading2, Paragraph } from '../../widget/Text'
import SpacingView from '../../widget/SpaceView';
import DetailCell from '../../widget/DetailCell';
export default class  MineScene extends PureComponent{
    static navigationOptions =({navigation})=>({
        headerRight:(
            <View style={{flexDirection:'row'}}>
                <NavigationItem
                    icon={require('../../img/Mine/icon_navigationItem_set_white.png')}
                    onPress={()=>{
                        alert('set')
                    }}
                />
                <NavigationItem
                    icon={require('../../img/Home/icon_navigationItem_message_white.png')}
                    onPress={()=>{
                       alert('message')
                    }}
                />
            </View>
        ),
        headerStyle:{backgroundColor:color.theme}
    })
    // 构造
    constructor(props) {
        super(props);
        // 初始状态
        this.state = {
            isRefreshing: false
        }
        // this.renderHeader= this.renderHeader.bind(this);
        // this.renderCell = this.renderCell.bind(this);
        this.onHeaderRefresh = this.onHeaderRefresh.bind(this);
        this.mineInfo = this.mineInfo.bind(this);
    }
    onHeaderRefresh() {
        this.setState({ isRefreshing: true })

        setTimeout(() => {
            this.setState({ isRefreshing: false })
        }, 2000);
    }
    mineInfo(){
        this.props.navigation.navigate('MineInfo');
    }
    renderHeader() {
        return (
            <View style={{backgroundColor: color.theme, paddingBottom: 20}}>
                <TouchableOpacity onPress={this.mineInfo} >
                <View style={styles.headerContent}>
                    <Image
                        source={require('../../img/Mine/avatar.png')}
                        style={styles.avatar}/>
                    <View>
                        <View style={{flexDirection: 'row', marginTop: 5}}>
                            <Heading1 style={{color: 'white'}}>名称</Heading1>
                            <Image
                                source={require('../../img/Mine/beauty_technician_v15.png')}
                                style={{marginLeft: 4}}
                            />
                        </View>
                        <Paragraph style={{color: 'white', marginTop: 4}}>个人信息></Paragraph>
                    </View>
                </View>
                </TouchableOpacity>
            </View>
        )
    }
    renderCell(){
        let cells =[];
        let dataSource = this.getDataList();
        for (let i=0;i<dataSource.length;i++){
            let sublist = dataSource[i];
            for (let j=0;j<sublist.length;j++){
                let data = sublist[j];
                let cell = (
                    <DetailCell title={data.title}
                                image={data.image}
                                subtitle={data.subtitle}
                                key={data.title}
                    />
                );
                cells.push(cell);
            }
            cells.push(<SpacingView key={i}/>)
        }

        return(
            <View style={{flex:1}}>
                {cells}
            </View>
        );
    }
    render(){

        return (
            <View style={{flex:1,backgroundColor:color.background}}>
                <View style={{position:'absolute',height:screen.width/2,
                    backgroundColor:color.theme,width:screen.width
                }}/>
                    <ScrollView
                        refreshControl={
                            <RefreshControl refreshing={this.state.isRefreshing}
                                onRefresh={this.onHeaderRefresh}
                                            tintColor='gray'
                            />
                        }
                    >
                        {this.renderHeader()}
                        <SpacingView/>
                        {this.renderCell()}
                    </ScrollView>

            </View>
        );
    }

    getDataList() {
        return (
            [
                [
                    { title: '我的钱包', subtitle: '办信用卡', image: require('../../img/Mine/icon_mine_wallet.png') },
                    { title: '余额', subtitle: '￥95872385', image: require('../../img/Mine/icon_mine_balance.png') },
                    { title: '抵用券', subtitle: '63', image: require('../../img/Mine/icon_mine_voucher.png') },
                    { title: '会员卡', subtitle: '2', image: require('../../img/Mine/icon_mine_membercard.png') }
                ],
                [
                    { title: '好友去哪', image: require('../../img/Mine/icon_mine_friends.png') },
                    { title: '我的评价', image: require('../../img/Mine/icon_mine_comment.png') },
                    { title: '我的收藏', image: require('../../img/Mine/icon_mine_collection.png') },
                    { title: '会员中心', subtitle: 'v15', image: require('../../img/Mine/icon_mine_membercenter.png') },
                    { title: '积分商城', subtitle: '好礼已上线', image: require('../../img/Mine/icon_mine_member.png') }
                ],
                [
                    { title: '客服中心', image: require('../../img/Mine/icon_mine_customerService.png') },
                    { title: '关于美团', subtitle: '我要合作', image: require('../../img/Mine/icon_mine_aboutmeituan.png') }
                ]
            ]
        )
    }
}
const styles = StyleSheet.create({
   avatar:{
       width:50,
       height:50,
       borderRadius:25,
       marginRight:10,
       borderWidth: 2,
       borderColor: '#51D3C6'
   },
    headerContent:{
       flexDirection:'row',
        margin:10
    }
});