/**
 * Created by sunbiao on 2017/8/3.
 */
import React, { PureComponent } from 'react'
import { View, Text, StyleSheet, Image, TouchableOpacity } from 'react-native'

import {Heading2} from '../../widget/Text'
import  screen from '../../common/screen'

export default class OrderItem extends  PureComponent{

    render(){
        let icon = this.props.icon;
        return(

                <TouchableOpacity style={styles.container} onPress={()=>this.props.onPress(this.props.selectedIndex)}>
                <Image source={icon} resizeMode='contain' style={styles.icon}/>
                <Heading2>{this.props.title}</Heading2>
                </TouchableOpacity>

        );
}
}
const styles = StyleSheet.create({
    container:{
        backgroundColor:'white',
        justifyContent:'center',
        alignItems:'center',
        width:screen.width/4,
        height:screen.width/5
    },
    icon:{
        width:30,
        height:30,

    }

})