/**
 * Created by sunbiao on 2017/7/24.
 */
import React, { Component } from 'react';
import { View, Text, StyleSheet, ScrollView, TouchableOpacity, ListView, Image, InteractionManager } from 'react-native';

import { Heading1, Heading2, Paragraph, HeadingBig } from '../../widget/Text';

import api, { recommendUrlWithId, groupPurchaseDetailWithId } from '../../api';
import GroupPurchaseCell from '../GroupPurchase/GroupPurchaseCell';
import NavigationItem from '../../widget/NavigationItem';
import RefreshListView from '../../widget/RefreshListView';

import RefreshState from '../../widget/RefreshState';
import screen from '../../common/screen';
import color from '../../widget/color';

import Button from '../../widget/Button';
import Seperator from '../../widget/SpaceView';
import DetailCell from '../../widget/DetailCell';
import OrderItem from './OrderItem';
import SpaceView from '../../widget/SpaceView';

export default class  OrderScene extends Component{
    listView:ListView;
    static navigationOptions=({navigation})=>({
        headerTitle:'订单',
        headerStyle:{backgroundColor:'white'}
    });
    // 构造
    constructor(props) {
        super(props);
        let ds=new ListView.DataSource({rowHasChanged:(r1,r2)=>r1!==r2});

        // 初始状态
        this.state = {
            dataSource:ds.cloneWithRows([]),
            info:Object,
        };
        this.renderHeader = this.renderHeader.bind(this);
        this._onPress = this._onPress.bind(this);
        this.requestData = this.requestData.bind(this);
        this.renderRow = this.renderRow.bind(this);
    }

    componentDidMount() {
        InteractionManager.runAfterInteractions(
            this.listView.startHeaderRefreshing()
        )
    }
    renderHeader(){
        let titles =[
            {'title':'待付款','icon':require('../../img/Order/order_tab_need_pay.png')},
            {'title':'待使用','icon':require('../../img/Order/order_tab_need_use.png')},
            {'title':'待评价','icon':require('../../img/Order/order_tab_need_review.png')},
            {'title':'退款/售后','icon':require('../../img/Order/order_tab_needoffer_aftersale.png')},

        ];
        return (
            <View >
               <DetailCell title='我的订单' subtitle='全部订单' />
                <View style={styles.itemContainer}>
                {titles.map((title,i)=>(
                    <OrderItem
                        title={title.title}
                        key={i}
                        icon={title.icon}
                        selectedIndex={i}
                        onPress={this._onPress}
                    />
                ))}
                </View>
                <SpaceView/>
                <DetailCell title="我的收藏" subtitle='查看全部'/>
            </View>
        )
    }
    _onPress(i) {
        alert(i);
    }


    renderRow (rowData){
        return(
            <GroupPurchaseCell
                    info={rowData}
                    onPress={(rowData)=>{
                        this.props.navigation.navigate('Purchase',{info:rowData})
                    }}
            />
        );
    }
   async requestData(){
        try {
            let response = await fetch(api.recommend);
            let json = await response.json();
            let dataSource = json.data.map((info)=>{
                return{
                    id: info.id,
                    imageUrl: info.squareimgurl,
                    title: info.mname,
                    subtitle: `[${info.range}]${info.title}`,
                    price: info.price
                }
            })
            this.setState({
                dataSource: this.state.dataSource.cloneWithRows(dataSource)
            })
            setTimeout(() => {
                this.listView.endRefreshing(RefreshState.NoMoreData)
            }, 500)
        }catch (error){
            this.listView.endRefreshing(RefreshState.Failure)
        }
   }
    render(){

        return (
            <View style={{flex:1,backgroundColor:'white'}}>
            <RefreshListView
                ref={(e)=>this.listView=e}
                dataSource={this.state.dataSource}
                renderHeader={this.renderHeader}
                renderRow={this.renderRow}
                onHeaderRefresh={this.requestData}
               />
            </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },
    itemContainer: {
        flexDirection: 'row',
    },
});