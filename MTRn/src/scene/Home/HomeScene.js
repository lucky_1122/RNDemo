/**
 * Created by sunbiao on 2017/7/24.
 */
import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Image,
    Dimensions,
    TextInput,
    ScrollView,
    FlatList,
    SectionList,
    Button,
    Alert,
    TouchableOpacity,
    StatusBar,
} from 'react-native';
import screen from '../../common/screen';
import color from '../../widget/color';
import NavigationItem from '../../widget/NavigationItem';
import { Paragraph,Heading1 } from '../../widget/Text';
import api from '../../api';
import HomeMenuView from './HomeMenuView';
import SpaceView from '../../widget/SpaceView';
import HomeGridView from './HomeGridView';
import GroupPurchaseCell from '../GroupPurchase/GroupPurchaseCell';

export default class  HomeScene extends Component{
    // 构造
      constructor(props) {
        super(props);
        // 初始状态
        this.state = {
            discounts:[],
            dataList: [],
            refreshing: false,
        };
        this.onCellSelected = this.onCellSelected.bind(this);
        this.onMenuSelected = this.onMenuSelected.bind(this);
        this.requestDiscount=this.requestDiscount.bind(this);
        this.requestData=this.requestData.bind(this);
        this.onGridSelected = this.onGridSelected.bind(this);
        this.requestRecommend = this.requestRecommend.bind(this);
        this.keyExtractor = this.keyExtractor.bind(this);
        this.itemSeparatorComponent = this.itemSeparatorComponent.bind(this);
          this.renderCell = this.renderCell.bind(this);
      }
    static  navigationOptions =({navigation})=>({
        headerTitle:(
            <TouchableOpacity style={styles.search}>
                <Image source={require('../../img/Home/search_icon.png')} style={styles.searchIcon}/>

                <Paragraph>一点点</Paragraph>
        </TouchableOpacity>),
        headerLeft:(
            <NavigationItem
                title='上海'
                titleStyle={{color:'white'}}
                onPress={()=>{

                }}
            />
        ),
        headerRight:(
          <NavigationItem
              icon={require('../../img/Home/icon_navigationItem_message_white.png')}
              style={styles.searchIcon}
              onPress={()=>{}}
          />
        ),
        headerStyle:{backgroundColor:color.theme}

    })

    componentDidMount() {
    this.requestData();
      }
    requestData(){
          this.setState({
              refreshing:true
          });
          this.requestDiscount();
          this.requestRecommend();
    }
   async requestDiscount() {
       try {
           let response = await fetch(api.discount)
           let json = await response.json()
           this.setState({discounts: json.data})
       } catch (error) {
           alert(error)
       }
   }
async requestRecommend(){
        try {
        let response = await fetch(api.recommend);
        let json = await response.json();
        let dataList = json.data.map((info)=>{
            return {
                id: info.id,
                imageUrl: info.squareimgurl,
                title: info.mname,
                subtitle: `[${info.range}]${info.title}`,
                price: info.price
            }
        });
        this.setState({
            refreshing:false,
            dataList:dataList,
        })
        }catch (error){
            this.setState({ refreshing: false })
        }
}
    renderHeader(){
        return(
            <View>
                <HomeMenuView menuInfos={api.menuInfo} onMenuSelected={this.onMenuSelected}/>
                <SpaceView/>
                <HomeGridView infos={this.state.discounts} onGridSelected={this.onGridSelected}/>
                <SpaceView/>
                <View style={styles.recommendHeader}><Heading1>猜你喜欢</Heading1></View>
            </View>
        );
    }
    onMenuSelected(index){
        // Alert.alert(index);
        Alert.alert('你点击了'+index);
        // alert(index);
    };
    onGridSelected(index){
        let discount = this.state.discounts[index]

        if (discount.type == 1) {
            StatusBar.setBarStyle('default', false)

            let location = discount.tplurl.indexOf('http')
            let url = discount.tplurl.slice(location)
            this.props.navigation.navigate('Web', { url: url })
        }
    }
    keyExtractor(item,index){
        return item.id;
    }
    renderCell(info: Object) {
        return (
            <GroupPurchaseCell
                info={info.item}
                onPress={this.onCellSelected}
            />
        )
    }

    onCellSelected(info: Object) {
        StatusBar.setBarStyle('default', false)
        this.props.navigation.navigate('Purchase', { info: info })
    }

    itemSeparatorComponent() {
        return (
            <View style={{height: screen.onePixel, backgroundColor: color.border, width: screen.width}}/>
        )
    }

    render(){

        return (
            <View style={styles.container}>
               <FlatList
                   data={this.state.dataList}
                   keyExtractor={this.keyExtractor}
                    ListHeaderComponent={this.renderHeader.bind(this)}
                   onRefresh={this.requestData}
                   refreshing={this.state.refreshing}
                   renderItem={this.renderCell}
                   ItemSeparatorComponent={this.itemSeparatorComponent}
               />
            </View>
        );
    }
}
const styles = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:color.background
    },
    recommendHeader: {
        height: 35,
        justifyContent: 'center',
        borderWidth: screen.onePixel,
        borderColor: color.border,
        // paddingVertical: 8,
        paddingLeft: 20,
        backgroundColor: 'white'
    },
    search:{
        width:screen.width*0.7,
        height:30,
        borderRadius:15,
        flexDirection:'row',
        justifyContent:'center',
        alignItems:'center',
        backgroundColor:'white',
        alignSelf:'center',

    },
    searchIcon:{
        width:20,
        height:20,
        margin:5,
    },

})