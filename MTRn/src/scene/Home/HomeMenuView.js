/**
 * Created by sunbiao on 2017/7/28.
 */
import React, { PureComponent } from 'react';
import { View, Text, StyleSheet, ScrollView, } from 'react-native';
import HomeMenuItem from './HomeMenuItem';

import screen  from '../../common/screen';
import PageControl from '../../widget/PageControl';
import  color from '../../widget/color'

export default class HomeMenuView extends PureComponent{
    // 构造
      constructor(props) {
        super(props);
        // 初始状态
        this.state = {
            currentPage:0
        };

      }
    onScroll(e){
        let x=e.nativeEvent.contentOffset.x;
        let currentPage = Math.round(x/screen.width);
        if (this.state.currentPage != currentPage){
            this.setState({
                currentPage:currentPage
            })
        }
    }
    render(){
          let {menuInfos,onMenuSelected} = this.props;
          let menuItems = menuInfos.map((info, index)=>(
             <HomeMenuItem
                key={info.title}
                title={info.title}
                icon={info.icon}
                onPress={()=>{onMenuSelected && onMenuSelected(index)}}
             />
          ))
        let menuViews = [];
          let pageCount = Math.ceil(menuItems.length/10);
          for (let  i=0;i<pageCount;i++){
              let length = menuItems.length<(i*10)?menuItems.length-(i*10):10;
              let items = menuItems.slice(i*10,i*10+length);
              let menuView = (
                  <View style={{flexDirection:'row',flexWrap:'wrap',width:screen.width}}
                        key={i}
                  >
                      {items}
                      
                  </View>
              );
              menuViews.push(menuView);
          }

        return(
            <View style={{backgroundColor:'white'}}>
                <ScrollView
                    horizontal={true}
                    showsHorizontalScrollIndicator={false}
                    pagingEnabled={true}
                    onScroll={(e)=>this.onScroll(e)}
                >
                    <View style={{flexDirection:'row'}}>
                    {menuViews}
                    </View>

                </ScrollView>
                <PageControl
                    style={styles.pageControl}
                    numberOfPages={pageCount}
                    currentPage={this.state.currentPage}
                    hidesForSinglePage
                    pageIndicatorTintColor='gray'
                    currentPageIndicatorTintColor={color.theme}
                    indicatorSize={{ width: 8, height: 8 }}
                />
            </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
    },
    contentContainer: {
    },
    menuContainer: {
        flexDirection: 'row',
    },
    itemsView: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        width: screen.width,
    },
    pageControl: {
        margin: 10,
    }
});