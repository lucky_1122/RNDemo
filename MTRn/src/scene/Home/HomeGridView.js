/**
 * Created by sunbiao on 2017/7/28.
 */
import React, { PureComponent } from 'react';
import { View, Text, StyleSheet } from 'react-native';

import color from '../../widget/color';
import screen from '../../common/screen';
import HomeGridItem from './HomeGridItem';

export default class HomeGridView extends PureComponent {
    render(){
        let infos = this.props.infos;
        let views = infos.map((info,i)=>(
            <HomeGridItem
                info={info}
                key={i}
                onPress={()=>this.props.onGridSelected(i)}
            />
        ))

        return(
            <View style={styles.container}>
                {views}
            </View>
        );
    }
}
const styles = StyleSheet.create({
    container:{
        flexDirection:'row',
        flexWrap:'wrap',
        justifyContent:'space-between',
        borderTopWidth:screen.onePixel,
       borderLeftWidth:screen.onePixel,
        borderColor:color.border
    }
})