/**
 * Created by sunbiao on 2017/8/2.
 */
import React, { PureComponent } from 'react'
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native'
import {Paragraph} from '../../widget/Text'
import screen from '../../common/screen'
import color from '../../widget/color'

export default class NearByHeaderView extends PureComponent{


    render(){
        let titles = this.props.titles;
        return(
            <View style={styles.container}>
                {titles.map((title,i)=>(
                    <TouchableOpacity
                        style={[{backgroundColor:this.props.selectedIndex== i ? '#FE566D':'white'},styles.item]}
                        key={i}
                        onPress={()=>this.props.onSelected(i)}
                    >
                        <Paragraph style={{color:this.props.selectedIndex ==i ? 'white':'#555555'}}>{title}</Paragraph>
                    </TouchableOpacity>
                ))}

            </View>
        );
    }
}
const styles = StyleSheet.create({
    container:{
        flexDirection:'row',
        flexWrap:'wrap'
    },
    item:{
        width:screen.width/4-10,
        height:30,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 15,
        borderWidth: screen.onePixel,
        borderColor: color.border,
        marginTop:5,
        marginBottom:5,
        marginLeft:8

    }
})