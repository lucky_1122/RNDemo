/**
 * Created by sunbiao on 2017/7/24.
 */
import React, { Component } from 'react';
import ScrollableTabView,{DefaultTabBar,ScrollableTabBar} from 'react-native-scrollable-tab-view';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Image,
    Dimensions,
    TextInput,
    ScrollView,
    FlatList,
    SectionList,
    TouchableOpacity,
    Button
} from 'react-native';
import { Heading1, Heading2, Paragraph } from '../../widget/Text';
import color from '../../widget/color';
import screen from '../../common/screen';
import NearByListScene from './NearByListScene';
import NavigationItem from '../../widget/NavigationItem';

export default class  NearbyScene extends Component{
    static navigationOptions =({navigation})=>({
        headerLeft:(
            <TouchableOpacity style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', padding: 10 }}>
                <Image style={{ width: 13, height: 16 }} source={require('../../img/Public/icon_food_merchant_address.png')} />
                <Text style={{ fontSize: 15, color: '#333333' }}> 上海 杨浦区</Text>
            </TouchableOpacity>
        ),
        headerRight:(
            <TouchableOpacity style={styles.searchBar}>
                <Image source={require('../../img/Home/search_icon.png')} style={styles.searchIcon} />
                <Paragraph>找附近的吃喝玩乐</Paragraph>
            </TouchableOpacity>
        ),
        headerStyle:{backgroundColor:'white'},
})

    // 构造
    constructor(props) {
        super(props);
        // 初始状态
        this.state = {};
    }
    render(){
        let titles=['享美食','住酒店','爱玩乐','全部'];
        let types = [
            ['热门', '面包甜点', '小吃快餐', '川菜', '日本料理', '韩国料理', '台湾菜', '东北菜'],
            ['热门', '商务出行', '公寓民宿', '情侣专享', '高星特惠', '成人情趣'],
            ['热门', 'KTV', '足疗按摩', '洗浴汗蒸', '大宝剑', '电影院', '美发', '美甲'],
            []
        ]
        return (
            <ScrollableTabView
                style={styles.container}
                tabBarBackgroundColor='white'
                tabBarActiveTextColor='#FE566D'
                tabBarInactiveTextColor='#555555'
                tabBarTextStyle={styles.tabBarText}
                tabBarUnderlineStyle={styles.tabBarUnderline}
                renderTabBar={()=><DefaultTabBar/>}
            >
                {titles.map((title,i)=>(

                    <NearByListScene
                        tabLabel={title}
                        key={i}
                        types={types[i]}
                         navigation={this.props.navigation}
                    />

                ))}

            </ScrollableTabView>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: color.background
    },
    searchBar: {
        width: screen.width * 0.65,
        height: 30,
        borderRadius: 19,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#eeeeee',
        alignSelf: 'flex-end',
        marginRight: 20,
    },
    searchIcon: {
        width: 20,
        height: 20,
        margin: 5,
    },
    tabBarText: {
        fontSize: 14,
        marginTop: 13,
    },
    tabBarUnderline: {
        backgroundColor: '#FE566D'
    },
});