/**
 * Created by sunbiao on 2017/8/2.
 */
import React,{PureComponent} from 'react';
import  {StyleSheet,ListView,InteractionManager,View} from  'react-native';
import RefreshListView from '../../widget/RefreshListView';
import RefreshState from '../../widget/RefreshState';
import api from '../../api'
import NearByCell from '../GroupPurchase/GroupPurchaseCell';
import NearByHeaderView from './NearByHeaderView';
import Sepeter from '../../widget/Separator';

export default class NearByListScene extends PureComponent{
    listView:ListView;
    // 构造
      constructor(props) {
        super(props);
        let ds = new ListView.DataSource({rowHasChanged:(r1,r2)=>r1!==r2});
        // 初始状态
        this.state = {
            dataSource:ds.cloneWithRows([]),
            typeIndex:0,
        };
        this.requestData = this.requestData.bind(this);
      }

    componentDidMount() {
        InteractionManager.runAfterInteractions(()=>{
            this.listView.startHeaderRefreshing();
        })
    }
    async requestData(){
          try {
              let response = await fetch(api.recommend);
              let json = await response.json();
              let dataSource = json.data.map((info)=>{
                  return{
                      id: info.id,
                      imageUrl: info.squareimgurl,
                      title: info.mname,
                      subtitle: `[${info.range}]${info.title}`,
                      price: info.price
                  }
              });
              dataSource.sort(() => { return 0.5 - Math.random() })

              this.setState({
                  dataSource:this.state.dataSource.cloneWithRows(dataSource),
              });
              setTimeout(()=>{
                  this.listView.endRefreshing(RefreshState.NoMoreData);
              },1000)
          }catch (error){
                    console.log(error);
                  this.listView.endRefreshing(RefreshState.NoMoreData)

          }
    }
      render(){
          return(

              <RefreshListView
                    ref={(e)=>this.listView=e}
                    dataSource={this.state.dataSource}
                    renderHeader={()=>(
                        <NearByHeaderView
                            titles={this.props.types}
                            selectedIndex={this.state.typeIndex}
                            onSelected={(index)=>{
                                if (index != this.state.typeIndex){
                                    this.setState({
                                        typeIndex:index,
                                    });
                                    this.listView.startHeaderRefreshing();
                                }
                            }}
                        />
                    )}
                    renderRow={(rowData)=>(
                        <View>
                          <NearByCell
                               info={rowData}
                               onPress={()=>{
                                 this.props.navigation.navigate('Purchase',{info:rowData})
                             }}
                          />
                            <Sepeter/>
                        </View>
                    )}

                    onHeaderRefresh={this.requestData}
                    />
          );
      }
}