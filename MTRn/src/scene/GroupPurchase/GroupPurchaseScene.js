/**
 * Created by sunbiao on 2017/8/1.
 */
import React, { PureComponent } from 'react'
import { View, Text, StyleSheet, ScrollView, TouchableOpacity, ListView, Image, InteractionManager } from 'react-native';

import { Heading1, Heading2, Paragraph, HeadingBig } from '../../widget/Text';

import api, { recommendUrlWithId, groupPurchaseDetailWithId } from '../../api';
import GroupPurchaseCell from './GroupPurchaseCell';
import NavigationItem from '../../widget/NavigationItem';
import RefreshListView from '../../widget/RefreshListView';

import RefreshState from '../../widget/RefreshState';
import screen from '../../common/screen';
import color from '../../widget/color';
import Button from '../../widget/Button';



export default class GroupPurchaseScene extends PureComponent{
    listView:ListView;
    static navigationOptions={
        headerTitle:'团购详情',
        headerStyle:{backgroundColor:'white'},
        headerRight:(
            <NavigationItem
                icon={require('../../img/Public/icon_navigationItem_share.png')}
                onPress={()=>{}}
            />
        ),
    }
    // 构造
     constructor(props) {
       super(props);
       let ds = new ListView.DataSource({rowHasChanged:(r1,r2)=>r1!==r2})
       // 初始状态
       this.state = {
           info:Object,
           dataSource:ds.cloneWithRows([]),
       };
       this.renderCell = this.renderCell.bind(this);
       this.requestList = this.requestList.bind(this);
       this.requestRecommend = this.requestRecommend.bind(this);
       this.renderHeader = this.renderHeader.bind(this);
     }

    componentDidMount() {
        InteractionManager.runAfterInteractions(()=>{
            this.listView.startHeaderRefreshing();
        })
    }
    renderCell(rowData){
        return (
            <GroupPurchaseCell
                info={rowData}
                onPress={this.props.navigation.navigate('Purchase',{info:rowData})}
            />
        );
    }
    renderHeader(){
        let info = this.props.navigation.state.params.info;
        return(
            <View>
                <View>
                    <Image source={{uri:info.imageUrl.replace('w.h','480.0')}} style={styles.banner}/>
                    <View style={styles.topContainer}>
                        <Heading1 style={{color:color.theme}}>¥</Heading1>
                        <HeadingBig style={{color:color.theme,marginBottom:-8}}>{info.price}</HeadingBig>
                        <Paragraph style={{marginLeft:10}}>门市价:  ¥{(info.price*1.1).toFixed(0)}</Paragraph>
                        <View style={{alignItems:'flex-end',flex:1}}>
                        <Button title="立即抢购"
                                style={{color:'white',fontSize:18}}
                                containerStyle={styles.buyButton}
                                onPress={()=>{}}/>
                        </View>
                    </View>
                </View>
            </View>
        );
    }
    requestList(){
        this.requestRecommend();
    }
    async requestRecommend(){
        try {
            let info = this.props.navigation.state.params.info;
            let response = await fetch(recommendUrlWithId(info.id));
            let json = await response.json();
            let dataSource = json.data.deals.map((data)=>{
                return {
                    id: data.id,
                    imageUrl: data.imgurl,
                    title: data.brandname,
                    subtitle: `[${data.range}]${data.title}`,
                    price: data.price
                }
            });
            this.setState({
                dataSource:this.state.dataSource.cloneWithRows(dataSource),
            });
            setTimeout(()=>{
               this.listView&& this.listView.endRefreshing(RefreshState.NoMoreData);
            },1000)

        }catch (error){
            this.listView&& this.listView.endRefreshing(RefreshState.Failure);
        }
    }
    render(){

        return(
            <View style={styles.container}>
                <RefreshListView
                    ref={(e) => this.listView = e}
                    dataSource={this.state.dataSource}
                     renderHeader={() => this.renderHeader()}
                    renderRow={(rowData) =>
                        <GroupPurchaseCell
                            info={rowData}
                            onPress={() =>{
                                this.props.navigation.navigate('Purchase', { info: rowData })
                            } }
                        />
                    }
                    onHeaderRefresh={this.requestList}
                />


            </View>
        );
    }
}
const styles = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:'white',
    },
    banner:{
        width:screen.width,
        height:screen.width*0.5
    },
    topContainer:{
        flexDirection:'row',
        padding:10,
        alignItems:'flex-end'
    },
    buyButton: {
        backgroundColor: '#fc9e28',
        width: 94,
        height: 36,
        borderRadius: 7,
    },
})